/*
 * The MIT License
 *
 * Copyright 2017 Chris Skevis <admin@skevis.org>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package assembler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * After some frustration and testing i decided to write an assembler so that 
 * i can perform system checks by making my own programs
 * 
 * The way it works is that you write in CHIP-8 "assembly", this assembler 
 * reads that file, makes a byte Array out of it and then dumps it to a bin
 * 
 */
public class yajc8a {
    
public static void main(String[] args) {
        File file = new File(args[0]);
        byte[] theByteArray = null;
        
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            line = br.readLine();
            //while ((line = br.readLine()) != null) {
            //    System.out.println(line);
            //}
            
            theByteArray = line.getBytes();
            
            for(int i=0;i<theByteArray.length;i++)
                System.out.print(Integer.toString(theByteArray[i],16));
                
        } catch (FileNotFoundException ex) {
            Logger.getLogger(yajc8a.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(yajc8a.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        try {
            FileOutputStream stream = new FileOutputStream("TEST2.txt");
            stream.write(theByteArray);
            stream.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(yajc8a.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(yajc8a.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
