/*
 * The MIT License
 *
 * Copyright 2017 Chris Skevis <admin@skevis.org>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package yajc8emu;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
        
/**
 *
 * Implements the RAM Module of CHIP - 8
 * 
 * Memory Map:
 * +---------------+= 0xFFF (4095) End of Chip-8 RAM
 * |               |
 * |               |
 * |               |
 * |               |
 * |               |
 * | 0x200 to 0xFFF|
 * |     Chip-8    |
 * | Program / Data|
 * |     Space     |
 * |               |
 * |               |
 * |               |
 * +- - - - - - - -+= 0x600 (1536) Start of ETI 660 Chip-8 programs
 * |               |
 * |               |
 * |               |
 * +---------------+= 0x200 (512) Start of most Chip-8 programs
 * | 0x000 to 0x1FF|
 * | Reserved for  |
 * |  interpreter  |
 * +---------------+= 0x000 (0) Start of Chip-8 RAM
 * 
 * https://en.wikipedia.org/wiki/CHIP-8
 * 
 * TODO : The uppermost 256 bytes (0xF00-0xFFF) are reserved for display refresh
 * and the 96 bytes below that (0xEA0-0xEFF) were reserved for call stack, 
 * internal use, and other variables.
 * 
 * TODO : there is no need for any of the lower 512 bytes memory space to be used, 
 * but it is common to store font data in those lower 512 bytes (0x000-0x200).
 * 
 */
public final class c8ram {
    
    /** 
    * 
    * Set the ram size, on CHIP-8 that is a fixed 4k 
    * 4096 bytes = 0x1000 in hex    
    * 
    * short = 16bit Signed in Java, we need this set to short instead of byte 
    * as there are no unsigned types in Java
    * 
    * */
    private short[] RAM = new short[0x1000];
    
    /**
     * Initializes a Chip 8 RAM Object
     *
     * @param rom_name the ROM name to load in RAM
     */    
    c8ram(String Rom_name){
        System.out.println("Assigning Memory...");
        
        // Creating a class Loader in order to handle resources
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream romFileStream = classLoader.getResourceAsStream("resources/roms/" + Rom_name);
        
        // Trying to load the file into a byte array before transfering it to 
        // the short[] Array which we consider as RAM
        try {
            System.out.println("Loading " + Rom_name + " into RAM");
            
            loadByteArrayIntoRam(getBytesFromInputStream(romFileStream), 0x200);
            
            
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
        
    }
    
    /**
    * Loads Bytes into short[]
    *
    * @param dataArray the Byte Array
    * @param offset Where to start inserting the Data
    * 
    * TODO : Most programs start at 0x200, but some on 0x600 ( ETI 660 )
    * TODO : Check if the rom we try to load is bigger than RAM
    * 
    */  
    public void loadByteArrayIntoRam(byte[] dataArray, int offset){
        for (byte data : dataArray) {
            /* & 0xFF is used in order to keep the unsigned value
            * https://stackoverflow.com/a/19061591
            *
            *   -1: 11111111 1111111
            *   0xFF: 00000000 1111111
            *   ======================
            *   255: 00000000 1111111
            */

            this.RAM[offset++] = (short)(data & 0xFF);
        }
    }
    
    /**
    *
    * Returns the short value at ram Position
    * 
    * @param position int the position
    * @return short 
    * 
    * TODO : No program should access 0x000 to 0x1FF as this was originally 
    *        Set as interpreter space
    * 
    * TODO : check if position is out of array bounds
    */
    public short readRamAt(int position){
        return this.RAM[position];
    }
    
    /**
    *
    * Writes a short value at ram Position
    * 
    * @param position int, the position
    * @param value short, the value
    * 
    * @return boolean success 
    * 
    * TODO : No program should access 0x000 to 0x1FF as this was originally 
    *        Set as interpreter space
    */
    public boolean writeRamAt(int position, short value){
        this.RAM[position] = value;
        
        return true;
    }
    
    
    /**
    * Turns Input Streams into a byte Array
    *
    * @param is InputStream to turn into Array
    * 
    * @return byte[] Data from the file
    */     
    private byte[] getBytesFromInputStream(InputStream is) throws IOException
    {
        // Setting up an empty byte array in order to set up the data
        byte[] to_return = new byte[0];
        
        // Working on 64l steps, inputing from the InputStream to the ByteArray
        try (ByteArrayOutputStream os = new ByteArrayOutputStream();) {
            byte[] buffer = new byte[0xFFFF];

            for (int len; (len = is.read(buffer)) != -1;){
                os.write(buffer, 0, len);
            }
            
            os.flush();

            to_return = os.toByteArray();
        }
        catch(IOException ex){
            System.err.println("Could not read Bytes from InputStream :" 
                    + ex.getMessage());
        }
        
        // Returns the byte Array
        return to_return;
    }

}
