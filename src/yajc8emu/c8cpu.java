/*
 * The MIT License
 *
 * Copyright 2017 Chris Skevis <admin@skevis.org>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package yajc8emu;

import java.util.Random;

/**
 *
 * Implements the CPU Part of the CHIP-8 System
 * Including handling of Instructions
 * 
 * Instruction codenames lookup
 * nnn or addr - a 12 bit value, the lower 12 of an instruction
 * n or nibble - a 4 bit value, the lowest 4 bits of an instruction
 * x           - a 4 bit value, the lower 4 bits of the high byte of an instr
 * y           - a 4 bit value, the upper 4 bits of the low byte of an instr
 * kk or byte  - an 8-bit value, the lowest 8 bits of an instruction
 * 
 */
public class c8cpu {
 
    // CHIP-8 has 16 General purpose 8 bit Registers
    // Due to unsigned missing, we have to use short instead of byte,
    // Same on all other vars remaining
    // TODO : VF = V16 Should not be accessible by any program
    private short[] V = new short[16];
    
    // One more register to store Memory addresses
    // TODO : Make I accessible only on the lower ( rightmost ) 12 bits
    private short I;
    
    // Two special purpose 8 -bit registers for sound
    // TODO : When these are non 0 they should be decremented at a rate of 60 Hz
    private short s_delay = 0;
    private short s_timer = 0;
    
    // TODO : The Following registers should not be accessible by any program
    // Program Counter, 16-bit unsigned 
    private int PC = 0x200;
    // Needed for Debug output 
    private int debugPC = 0x200;
    
    // Stack Pointer, 16-bit unsigned 
    private int SP;
    
    // Stack, 16 level deep 
    // TODO : Implement stack overflow exception
    private int[] Stack = new int[16];
    
    // Random Generator
    Random randomGenerator = new Random();
    
    // Some control booleans here
    private boolean running = true;
    
    // RAM
    private c8ram RAM;
    // Keyboard
    private c8keys Keyboard;
    
    public c8cpu(String rom_name) {
        // TODO : Decide if we want to start at 0x200 or somewhere else
        this.PC = 0x200;
        this.RAM = new c8ram(rom_name);
        this.Keyboard = new c8keys();
        this.SP = 0;
    }
    
    public void executeNextInstruction(){
        //Setting this for debugOutput 
        this.debugPC = this.PC;
        
        // Here comes some bitwise mess...
        // All instructions in Chip 8 are 2 bytes long = 16 bit
        // So we need an int to store that in java
        
        // Getting the first byte of the instruction...
        // 0000 0000 *0001 0010* on INVADERS on 0x200
        short upperInstruct = this.RAM.readRamAt(this.PC++);
        //System.out.println(upperInstruct + ": " + Integer.toString(upperInstruct,2));
        
        // Getting the second byte of the instruction...
        // 0000 0000 *0010 0101* on INVADERS on 0x201
        short lowerInstruct = this.RAM.readRamAt(this.PC++);
        //System.out.println(lowerInstruct + ": " + Integer.toString(lowerInstruct,2));
        
        // Merging into one instruction
        // Firstly we se the upper part on an int ( 32 bit )
        // 0000 0000 0000 0000 0000 0000 *0001 0010*
        int instruction = upperInstruct;
        //System.out.println(instruction + ": " + Integer.toString(instruction,2));
        
        // Then we push it 8 bits left to make space for the second byte
        // 0000 0000 0000 0000 *0001 0010* 0000 0000
        instruction = instruction << 8;
        //System.out.println(instruction + ": " + Integer.toString(instruction,2));
        
        // Then we use normal addition to merge the lower part
        // 0000 0000 0000 0000 *0001 0010* ~0010 0101~
        instruction += lowerInstruct;
        //System.out.println(instruction + ": " + Integer.toString(instruction,2));
        
        // Instruction building is now done...
        
        // We now have to get the opcode from that
        // Since we know we want to see if the opcode starts with 0-F 
        // we just need the first 4 bits to figue that out.
        
        // so we just have to & with 1111 0000 0000 0000 = 0xF000
        // dont forget to push that right 12 bits!
        int opcode = (instruction & 0xF000) >> 12;
        //System.out.println(opcode + ": " + Integer.toString(opcode,16));
        
        // At last we have what we need
        switch(opcode){
            case 0x0: sysClsRetCall(instruction);
                      break;
            case 0x1: jmpToAddr(instruction);
                      break;
            case 0x2: callSub(instruction);
                      break;
            case 0x3: skipIfEq(instruction);
                      break;
            case 0x4: skipIfNotEq(instruction);
                      break;
            case 0x5: skipIfRegEq(instruction);
                      break;
            case 0x6: putValueToReg(instruction);
                      break;
            case 0x7: addValueToReg(instruction);
                      break;
            case 0x8: handle8opcode(instruction);
                      break;
            case 0x9: skipIfRegsNE(instruction);
                      break;
            case 0xA: setItoAddr(instruction);
                      break;
            case 0xB: jmpToAddrReg(instruction);
                      break;
            case 0xC: setRndAndReg(instruction);
                      break;
            case 0xD: displaySprite(instruction);
                      break;
            case 0xE: handleEopcode(instruction);
                      break;
            case 0xF: handleFopcode(instruction);
                      break;
            default : System.out.println("Unknown opcode : 0x" + 
                      Integer.toString(opcode,16)); 
                      this.running = false;
                      break;
        }  
    }

    /**
     * Skip next instruction if Vx != Vy
     * 
     * 9xy0 - SNE Vx != Vy
     * 
     */
    private void skipIfRegsNE(int instruction) {
        short regX = getInstructionX(instruction);
        short regY = getInstructionX(instruction);
        
        if(this.V[regX] != this.V[regY])
            this.PC +=2;
        
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": SNE V" + Integer.toString(regX,16) + 
                ", V" + Integer.toString(regY,16)); 
    }
    
    
    /**
     * Sets a Register to the result of & of a Random number and KK
     * 
     * Cxkk - RND Vx, byte
     * 
     */
    private void setRndAndReg(int instruction) {
        short regX = getInstructionX(instruction);
        short value = getInstructionKK(instruction);
        this.V[regX] = (short)( this.randomGenerator.nextInt(255) & value);
        
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": RND V" + Integer.toString(regX,16) + 
                ", 0x" + Integer.toString(value,16)); 
    }
    
    /**
    * Handles All scenarios of 0xE opcode
    * 
    * @param instruction int, the full instruction
    * 
    */
    private void handleEopcode(int instruction) {
        // E opcode is decided on the lower 2 nibbles
        // So we & with 0000 0000 1111 1111 = 0xFF
        // But wait... this is the same as asking for kk :)
        short opcodeKK = getInstructionKK(instruction);
        switch(opcodeKK){
            case 0xA1 : skipIfKeyNotPressed(instruction);
                       break;
            case 0x9E : skipIfKeyPressed(instruction);
                       break;
            default : System.out.println("Unknown Ex Call 0x" +
                      Integer.toString(opcodeKK,16) ); 
                      this.running = false;
                      break;
        }
            
    }
    
     /**
     * Skip next Instruction if key with value Vx is pressed
     * 
     * Ex9E = SKP Vx
     * 
     */
    private void skipIfKeyPressed(int instruction) {
        short regX = getInstructionX(instruction);
        int key = this.V[regX];
        int keyPressed = this.Keyboard.getKeyPressed();
        if( keyPressed == key)
            this.PC +=2;
        
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": SKP V" + Integer.toString(regX,16)); 
    }
    
    /**
     * Skip next Instruction if key with value Vx is not pressed
     * 
     * ExA1 = SKNP Vx
     * 
     */
    private void skipIfKeyNotPressed(int instruction) {
        short regX = getInstructionX(instruction);
        
        int key = this.V[regX];
        int keyPressed = this.Keyboard.getKeyPressed();
        if( keyPressed != key)
            this.PC +=2;
        
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": SKNP V" + Integer.toString(regX,16)); 
    }
    
    /**
    * Handles All scenarios of 0x8 opcode
    * 
    * @param instruction int, the full instruction
    * 
    */
    private void handle8opcode(int instruction) {
        // 8 opcode is decided on the lower 1 nibble
        // So we & with 0000 0000 0000 1111 = 0xF
        // But wait... this is the same as asking for N :)
        short opcodeN = getInstructionN(instruction);
        switch(opcodeN){
            case 0x0 : setRegToReg(instruction);
                       break;
            case 0x1 : orRegToReg(instruction);
                       break;
            case 0x2 : andRegToReg(instruction);
                       break;
            case 0x3 : xorRegToReg(instruction);
                       break;
            case 0x4 : addCRegToReg(instruction);
                       break;
            case 0x5 : subCRegToReg(instruction);
                       break;
            default : System.out.println("Unknown 8xy Call 0x" +
                      Integer.toString(opcodeN,16) ); 
                      this.running = false;
                      break;
        }
            
    } 

    /**
     * performs a substruction of Vx and Vy, saves to Vx
     * Sets Vf to 1 if Vx > Vy, 0 otherwise
     * 
     * 8xy5 - Set Vx = Vx - Vy, No Borrow on Vf
     * 
     */
    private void subCRegToReg(int instruction) {
        short regX = getInstructionX(instruction);
        short regY = getInstructionY(instruction);
        
        if(this.V[regX] > this.V[regY]){
            this.V[0xF] = 1;
            this.V[regX] = (short)(this.V[regX] - this.V[regY]);
        }
        else {
            this.V[0xF] = 0;
            this.V[regX] = (short)(256 + (this.V[regX] - this.V[regY]));
        }
        
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": SUB V" + Integer.toString(regX,16) +
                ", V" + Integer.toString(regY,16)); 
    }
    
    /**
     * performs an addition of Vx and Vy, saves to Vx
     * Sets Vf to 1 if carry is needed
     * 
     * 8xy4 - Set Vx = Vx + Vy, Carry on Vf
     * 
     */
    private void addCRegToReg(int instruction) {
        short regX = getInstructionX(instruction);
        short regY = getInstructionY(instruction);
        int value = this.V[regX] + this.V[regY];

        if(value > 255){
            value -= 256;
            this.V[0xF] = 1;
        }
        this.V[regX] = (short)value;
        
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": ADD V" + Integer.toString(regX,16) +
                ", V" + Integer.toString(regY,16)); 
    }
    
    /**
     * performs a bitwise XOR on Vx and Vy, saves to Vx
     * 
     * 8xy3 - XOR Vx = Vx ^ Vy
     * 
     */
    private void xorRegToReg(int instruction) {
        short regX = getInstructionX(instruction);
        short regY = getInstructionY(instruction);
        this.V[regX] ^= this.V[regY];
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": XOR V" + Integer.toString(regX,16) +
                ", V" + Integer.toString(regY,16)); 
    }
    
    /**
     * performs a bitwise AND on Vx and Vy, saves to Vx
     * 
     * 8xy2 - AND Vx = Vx & Vy
     * 
     */
    private void andRegToReg(int instruction) {
        short regX = getInstructionX(instruction);
        short regY = getInstructionY(instruction);
        this.V[regX] &= this.V[regY];
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": AND V" + Integer.toString(regX,16) +
                ", V" + Integer.toString(regY,16)); 
    }
    
    /**
     * performs a bitwise OR on Vx and Vy, saves to Vx
     * 
     * 8xy1 - Set Vx = Vx | Vy
     * 
     */
    private void orRegToReg(int instruction) {
        short regX = getInstructionX(instruction);
        short regY = getInstructionY(instruction);
        this.V[regX] |= this.V[regY];
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": OR V" + Integer.toString(regX,16) +
                ", V" + Integer.toString(regY,16)); 
    }
    
    
     /**
     * Sets Register x at same value as Register y
     * 
     * 8xy0 = Set Vx = Vy
     * 
     */
    private void setRegToReg(int instruction) {
        short regX = getInstructionX(instruction);
        short regY = getInstructionY(instruction);
        this.V[regX] = this.V[regY];
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": LD V" + Integer.toString(regX,16) +
                ", V" + Integer.toString(regY,16)); 
    }
    
    /**
     * Calls as subroutine at address location
     * 
     * 2nnn - CALL addr
     * 
     */
    private void callSub(int instruction) {
        short addr = getInstructionNNN(instruction);
        this.Stack[this.SP++] = this.PC;
        this.PC = addr;
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": CALL #" + Integer.toString(this.PC,16)); 
    }
    
    /**
    * Handles All scenarios of 0xF opcode
    * 
    * @param instruction int, the full instruction
    * 
    */
    private void handleFopcode(int instruction) {
        // F opcode is decided on the lower 2 nibbles
        // So we & with 0000 0000 1111 1111 = 0xFF
        // But wait... this is the same as asking for kk :)
        short opcodeKK = getInstructionKK(instruction);
        switch(opcodeKK){
            case 0x07 : setRegAsTmr(instruction);
                        break;
            case 0x15 : setDelayTmr(instruction);
                        break;
            case 0x1E : setItosum(instruction);
                        break;
            case 0x65 : populateRegsFromRam(instruction);
                        break;
            case 0x33 : storeBCDinRam(instruction);
                        break;
            default : System.out.println("Unknown F Call 0x" +
                      Integer.toString(opcodeKK,16) ); 
                      this.running = false;
                      break;
        }
            
    }    
 
    
    /**
     * Stores DCB representation of Vx in memory locations I, I+1, I+2
     * 
     * Fx33 - LD B, Vx
     * 
     */
    private void storeBCDinRam(int instruction) {
        short regX = getInstructionX(instruction);
        short regValue = this.V[regX];
        // Ram at position I must contain the hundreds
        // Ram at position I+1 the tens
        // Ram at position I+2 the ones 
        
        this.RAM.writeRamAt(this.I, (short)(regValue / 100));
        this.RAM.writeRamAt((this.I + 1), (short)((regValue % 100) / 10));
        this.RAM.writeRamAt((this.I + 2), (short)((regValue % 100) % 10));
        
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": LD B, V" + Integer.toString(regX,16)); 
    }
    
    /**
     * Sets the Register at same value as Delay timer
     * 
     * Fx07 - LD Vx, DT
     * 
     * TODO : Fix Sound and Speed
     * 
     */
    private void setRegAsTmr(int instruction) {
        short regX = getInstructionX(instruction);
         this.V[regX] = this.s_delay;

        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": LD V" + Integer.toString(regX,16) + ", DT"); 
    }
    
    /**
     * Sets the Sound delay timer
     * 
     * Fx15 - LD DT, Vx
     * 
     * TODO : Fix Sound and Speed
     * 
     */
    private void setDelayTmr(int instruction) {
        short regX = getInstructionX(instruction);
        this.s_delay = this.V[regX];

        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": LD DT, V" + Integer.toString(regX,16)); 
    }
    
    /**
     * Reads registers V0 to Vx from memory starting at location I
     * 
     * Fx65 - LD Vx, [I]
     * 
     */
    private void populateRegsFromRam(int instruction) {
        short regs = getInstructionX(instruction);
        for(int i=0 ; i<=regs ; i++){
            this.V[i] = this.RAM.readRamAt(this.I + i);
        }
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": LD V" + Integer.toString(regs,16) +
                " [I]"); 
    }
    
     /**
     * Adds the value of a register to I
     * 
     * Fx1E - Set I = I + Vx
     * 
     * TODO : Handle overflow 
     * 
     */
    private void setItosum(int instruction) {
        short reg = getInstructionX(instruction);
        this.I = (short)(this.V[reg] + this.I);
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": ADD I," +  
                " V" + Integer.toString(reg,16)); 
    }
    
    
    /**
    * Displays Sprite on Screen
    * Dxyn - Display n-byte sprite starting at memory location I at Vx, Vy,
    *        Set VF = Collision.
    * 
    * @param instruction int, the full instruction
    * 
    * TODO : Actually draw something 
    * 
    */
    private void displaySprite(int instruction) {
        short start = this.I;
        short regX = getInstructionX(instruction);
        short regY = getInstructionY(instruction);
        short nibble = getInstructionN(instruction);
        
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": DRW V" + Integer.toString(regX,16) +
                ", V" + Integer.toString(regY,16) + 
                ", " + Integer.toString(nibble,16)); 
    }
    
    /**
    * Jumps to Address
    * 1nnn - Jump to Location nnn
    * 
    * @param instruction int, the full instruction
    * 
    */
    private void jmpToAddr(int instruction) {
        this.PC = getInstructionNNN(instruction);
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": JP #" + Integer.toString(this.PC,16)); 
    }
    
    /**
    * Jumps to Address plus V0
    * Bnnn - Jump to Location nnn + V0
    * 
    * @param instruction int, the full instruction
    * 
    */
    private void jmpToAddrReg(int instruction) {
        this.PC = getInstructionNNN(instruction) + this.V[0];
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": JP V0, #" + Integer.toString(this.PC,16)); 
    }
    
    /**
    * Set I Register to specific Address
    * 
    * Annn - Set I to nnn
    * 
    * @param instruction int, the full instruction
    * 
    */
    private void setItoAddr(int instruction) {
        this.I = getInstructionNNN(instruction);
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": LD I, #" + Integer.toString(this.I,16)); 
    }
    
    /**
    * Puts a value on a register.
    * 6xkk - Set V[x] = kk
    * 
    * @param instruction int, the full instruction
    * 
    */
    private void putValueToReg(int instruction) {
        short reg = getInstructionX(instruction);
        short value = getInstructionKK(instruction);
        this.V[reg] = value;
        System.out.println("0x" + Integer.toString(this.debugPC,16) + 
                ": LD V" + Integer.toString(reg,16) 
                + ", #" + Integer.toString(value)); 
    }

    /**
    * Skips next instruction if Vx = kk
    * 3xkk - Skip if Vx = kk
    * 
    * @param instruction int, the full instruction
    * 
    */
    private void skipIfEq(int instruction) {
        short reg = getInstructionX(instruction);
        short value = getInstructionKK(instruction);
        if( this.V[reg] == value )
            this.PC +=2;
        System.out.println("0x" + Integer.toString(this.debugPC,16) + 
                ": SE V" + Integer.toString(reg,16) + 
                ", #" + Integer.toString(value,16));  
    }
    
    /**
    * Skips next instruction if Vx != kk
    * 4xkk - Skip if Vx != kk
    * 
    * @param instruction int, the full instruction
    * 
    */
    private void skipIfNotEq(int instruction) {
        short reg = getInstructionX(instruction);
        short value = getInstructionKK(instruction);
        if(this.V[reg] != value)
            this.PC +=2;
        
        System.out.println("0x" + Integer.toString(this.debugPC,16) + 
                ": SNE V" + Integer.toString(reg,16) + 
                ", #" + Integer.toString(value,16)); 
    }
    
    /**
    * Skips next instruction if Vx = Vy
    * 5xy0 - Skip if Vx = Vy
    * 
    * @param instruction int, the full instruction
    * 
    */
    private void skipIfRegEq(int instruction) {
        short regX = getInstructionX(instruction);
        short regY = getInstructionY(instruction);
        if(this.V[regX] == this.V[regY])
            this.PC +=2;
        
        System.out.println("0x" + Integer.toString(this.debugPC,16) + 
                ": SE V" + Integer.toString(regX,16) + 
                ", V" + Integer.toString(regY,16)); 
    }
    
    /**
    * Handles SYS, CLS or RET Calls
    * 0nnn - Ignored
    * 00E0 - Clear display
    * 00EE - Return from subroutine 
    * 
    * @param instruction int, the full instruction
    * 
    */
    private void sysClsRetCall(int instruction) {
        // We know that the first 4 bits are 0, 
        // We need to know the other 12 now.
        // So we & with 0000 0000 1111 1111 = 0xFF
        // But wait... this is the same as asking for kk :)
        short opcodeKK = getInstructionKK(instruction);
        switch(opcodeKK){
            case 0xE0 : System.out.println("If i had a display i would clr it");
                        this.running = false;
                        break;
            case 0xEE : returnfromSub();                         
                        break;
            default : System.out.println("Unknown sysClsRetCall0x" +
                      Integer.toString(opcodeKK,16) );
                      this.running = false;
                      break;
        }
            
    }
    
     /**
     * Return from a subroutine. 
     * 
     * 00EE - RET
     * 
     * The interpreter sets the PC to the address at the top of the stack,
     * Then subtrack 1 from the stack pointer.
     * 
     */
    void returnfromSub() {
        this.PC = this.Stack[--this.SP];
        
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
        ": RET"); 
    }
    
    /**
     * Adds a value on a register
     * 
     * 7xkk - Set Vx = Vx + kk
     * 
     * TODO : Handle overflow 
     * 
     */
    private void addValueToReg(int instruction) {
        short reg = getInstructionX(instruction);
        short value = getInstructionKK(instruction);
        this.V[reg] = (short)(this.V[reg] + value);
        
        System.out.println("0x" + Integer.toString(this.debugPC,16) +
                ": ADD V" + Integer.toString(reg,16) +
                ", #" + value); 
    }
    
    private short getInstructionX(int instruction){
        // x are the lower 4 bits of the high byte of the instruction
        // for the int type that we use we need to & with
        // 0000 0000 0000 0000 *0000 1111 0000 0000* = 0xF00
        // and shift left 8 spaces
        return (short)((instruction & 0xF00) >> 8);
    }
    
    private short getInstructionY(int instruction){
        // y are the upper 4 bits of the low byte of the instruction
        // for the int type that we use we need to & with
        // 0000 0000 0000 0000 *0000 0000 1111 0000* = 0xF0
        // and shift left 4 spaces
        return (short)((instruction & 0xF0) >> 4);
    }
    
    private short getInstructionN(int instruction){
        // n are the lower 4 bits of the instruction
        // for the int type that we use we need to & with
        // 0000 0000 0000 0000 *0000 0000 0000 1111* = 0xF
        return (short)(instruction & 0xF);
    }
    
    private short getInstructionKK(int instruction){
        // kk is an 1 byte, the lowest 8 bits of the instruction
        // for the int type that we use we need to & with
        // 0000 0000 0000 0000 *0000 0000 1111 1111* = 0xFF
        return (short)(instruction & 0xFF);
    }
    
    private short getInstructionNNN(int instruction){
        // nnn are the lower 12 bits of the instruction
        // for the int type that we use we need to & with
        // 0000 0000 0000 0000 *0000 1111 1111 1111* = 0xFFF
        return (short)(instruction & 0xFFF);
    }
    
    /**
     * @return the PC
     */
    public int getPC() {
        return this.PC;
    }

    /**
     * @param aPC the PC to set
     */
    public void setPC(short aPC) {
        this.PC = aPC;
    }

    /**
     * @return the running
     */
    public boolean isRunning() {
        return this.running;
    }

    /**
     * @param running the running to set
     */
    public void setRunning(boolean running) {
        this.running = running;
    }
}
