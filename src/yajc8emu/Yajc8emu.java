/*
 * The MIT License
 *
 * Copyright 2017 Chris Skevis <admin@skevis.org>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package yajc8emu;

import debugger.ui.dash;

/**
 * 
 * Main Class of the emulator
 * 
 * Based on : http://devernay.free.fr/hacks/chip8/C8TECH10.HTM#0.1
 */
public class Yajc8emu {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Starting CHIP 8 VM...");
        
        c8cpu CPU = new c8cpu("INVADERS");

        while(CPU.isRunning()){
            CPU.executeNextInstruction();
        }
 
    }
    
}
